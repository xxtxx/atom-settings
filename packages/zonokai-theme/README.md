# Zonokai theme

A dark blue syntax theme for atom.

the idea of the theme was originally influenced from [Monokai](http://github.com/lvillani/el-monokai-theme) but kinda took its own path.

![alt text](http://github.com/ZehCnaS34/zonokai-theme/raw/master/zonokai.png)
