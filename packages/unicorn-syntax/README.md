# unicorn-syntax theme

A retro colored theme, with a slight unicorn flare to it.

![Unicorn Screenshot](http://i.imgur.com/o0ItRR8.png)
