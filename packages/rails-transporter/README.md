# Rails Transporter package [![Build Status](https://travis-ci.org/hmatsuda/rails-transporter.svg?branch=master)](https://travis-ci.org/hmatsuda/rails-transporter)

This package provides commands to open controller, view, model, helper template, asset and migration on Ruby on Rails on Atom editor.

Quickly open files or finder using following keymaps.

* `ctrl-alt-r c` - Open controller from model, view or controller-spec
* `ctrl-alt-r v` - Open view finder from controller or model
* `ctrl-alt-r l` - Open layout from view
* `ctrl-alt-r m` - Open model from controller, view or model-spec
* `ctrl-alt-r h` - Open helper from controller, view or helper-spec
* `ctrl-alt-r s` - Open spec from controller, helper or model
* `ctrl-alt-r p` - Open partial template from render method in view
* `ctrl-alt-r a` - Open asset from javascript_include_tag or stylesheet_link_tag method in view
* `ctrl-alt-r d m` - Open migration finder

![](http://cl.ly/image/0q2B370v3S3Y/out.gif)

## Requirement
* Ruby 1.9.3+
* Rails 2.0.0+
