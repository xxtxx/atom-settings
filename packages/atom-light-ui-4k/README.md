# Atom Light UI theme for 4k displays

Scales the default light UI theme for Atom by 1.5

![](https://f.cloud.github.com/assets/671378/2265022/bb148a20-9e7a-11e3-81c8-bf5965d48183.png)
