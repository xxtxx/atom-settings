# Atom Unicorn theme

Unicorn UI theme for Atom that matches the [unicorn syntax](https://atom.io/packages/unicorn-syntax)

This theme can then be activated by going to the _Themes_ section in the Settings view (`cmd-,`) and selecting it from the _UI Themes_ drop-down menu.

![Screenshot](http://i.imgur.com/EX5t2XM.png)
