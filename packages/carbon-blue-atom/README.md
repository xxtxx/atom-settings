# Carbon Blue Atom Syntax Theme

This is a syntax theme inspired by [CabonnightBlue] (http://colorsublime.com/theme/CarbonightBlue) for atom.

Install with `apm install carbon-blue-atom`.

![](http://alexgrant.info/img/screen-shot-carbon-blue-atom.png)
