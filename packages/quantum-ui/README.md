# Quantum UI Theme for Atom

<img src="https://dl.dropboxusercontent.com/u/27056280/Atom_theme.png" />

Quantum is a simple, flat theme for Atom based around the Tomorrow color scheme. **Pair it with the [Quantum Syntax Theme](https://github.com/mbullington/quantum-syntax)**.

Originally, somewhere under all this new CSS, Quantum originated and was forked from [neutron-ui](https://atom.io/themes/neutron-ui) by @brentd, which was forked from the Atom port of Spacegray. Oh, how nice open source software is.

## Notes:
  * If you have the Source Code Pro font installed, Quantum will default to it. If not, it'll use Monospace unless you provide another font via Atom's Settings.
