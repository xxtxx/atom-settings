# Ice Cold UI theme

Ice Cold UI theme for Atom.

A UI I built to go along with the Ice Cold syntax that I also made. Just something I'm
doing for fun that has fun colors that work well with my eyes. Going to update this over time
whenever I find bugs or want to change (or forget) something.

![A screenshot of your theme](https://github.com/tannadev/atom-ice-cold-ui/blob/master/ice_cold_ui.png?raw=true)
