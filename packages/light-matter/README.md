# Light Matter Syntax Theme

A beautiful light syntax theme for GitHub's editor, [Atom](https://atom.io/).

![](https://raw.githubusercontent.com/chrishtanaka/light-matter/master/screenshots/screenshot.png)

## Installation

#### Install w/ Atom

1. Navigate to `Atom -> Preferences...`
2. Select the `themes` tab
3. Type `light matter` in the search box
4. Find `Light Matter` from the search results and click `Install`

#### Install w/ Terminal
1. Make sure you have atom `shell commands` installed by navigating to `Atom -> Install Shell Commands`
2. Open a new terminal window
3. Type the command `apm install light-matter`

#### Activating the Theme

Navigate to `Atom -> Preferences...`, select the `themes` tab, and select `Light Matter`, from the `Syntax Theme` dropdown.

## Additional Customization

#### Font

Theme pairs well with [Source Code Pro](https://github.com/adobe/source-code-pro) Regular at size 13px.

#### UI Theme

[Light Matter UI](https://github.com/chrishtanaka/light-matter-ui.git)
