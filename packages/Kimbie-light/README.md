# Kimbie (light) for Atom

A color scheme inspired by [Cold Spring Fault Less Youth](http://www.discogs.com/Mount-Kimbie-Cold-Spring-Fault-Less-Youth/master/561611), the second album by British band Mount Kimbie.

![Animated Screenshot](https://raw.github.com/idleberg/atom-kimbie-light/master/screenshot.png)

## Installation

1. Change directory to `~/atom/packages/`
2. Clone repository `git clone https://github.com/idleberg/atom-kimbie-light.git`
3. Enable theme from the *Atom/Preferences* menu

See the official [documentation](https://atom.io/docs/latest/converting-a-text-mate-theme) for details

## License

This work by Jan T. Sott is licensed under a [Creative Commons Attribution-ShareAlike 4.0 Unported License](http://creativecommons.org/licenses/by-sa/4.0/deed.en_US).

## Donate

You are welcome support this project using [Flattr](https://flattr.com/submit/auto?user_id=idleberg&url=https://github.com/idleberg/atom-kimbie-light) or Bitcoin `17CXJuPsmhuTzFV2k4RKYwpEHVjskJktRd`